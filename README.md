# Projeto Bases de Dados

## Objetivo

O objetivo deste projeto é implementar uma base de dados para gerir a oferta dos serviços do C4G à comunidade. Esta base de dados irá guardar informação sobre quem são os membros do C4G e outros utilizadores externos que poderão estar ligados à infraestrutura. Informação sobre quais os recursos disponíveis e a sua quantidade dos mesmos deve também ser guardada nesta base de dados.

## Sobre o C4G 
O Colaboratório para as Geociências (C4G) é uma infraestrutura de investigação dedicada às Ciencias da Terra Sólida que agrega 58 laboratórios e 15 instituições em portugal. Mais informações no website: https://www.c4g-pt.eu/pt/

## TAREFAS A COMPLETAR

* Realizar o modelo de entidade e relacionamento
* Realizar o modelo relacional respetivo
* Realizar uma aplicação para satisfazer as necessidades do cliente
    * A aplicação será em JavaScript, HTML, CSS e PHP
* Realizar um relatório
    *  Explicação do trabalho e das decisões tomadas


## Relações entre os diferentes dados

#### Recursos

* Podem ter caracteristicas diferentes dependendo do recurso
* Tem que ser verificada a disponibilidade e custos

#### Serviços 

* São supervisionados por grupos de trabalho
* Têm sempre um responsável
* Têm sempre um utilizador que o requer
* Registo de cada serviço com lista descriminada e a quem foi prestado

#### Instituições

* Podem ser publicas, privadas ou um utilizador individual.

#### Grupos de trabalho

* Caracterizam os vários tipos de serviços disponíveis

#### Utilizadores

* Administradores
    * Podem adicionar ou remover serviços e equipamentos
    * São obrigatóriamente membros do C4G,portanto têm também todos os seus direitos
    * Podem ainda adicionar ou remover utilizadores (com o respetivo tipo de utilizador aqui definido)
* Membros do C4G
    * Podem pertencer ou não a instituições parceiras
    * Podem pertencer ou não a grupos de investigação
* Outros utilizadores 
    * Podem visualizar todas os serviços disponíveis

## Bootstrap
Iremos utilizar bootstrap para conseguirmos fazer o css mais rápidamente e melhor apresentável.

**Bootstrap CDN :**

    CSS : https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css
    JS : https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js




